﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project
{
    public class Borrow : Student
    {
        public override List<string> Category()
        {
            DataHandling xml = DataHandling.Instance();
            return xml.ReadCategory()["Borrow"];
        }
    }
}