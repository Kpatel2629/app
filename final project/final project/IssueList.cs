﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace final_project
{
    public class IssueList
    {
        private ObservableCollection<Issue> issues = null;

        public IssueList() => issues = new ObservableCollection<Issue>();

        public Issue this[int i]
        {
            get => issues[i];
            set => issues[i] = value;
        }
        public void Add(Issue a) => issues.Add(a);
        public void AddRange(ObservableCollection<Issue> a)
        {
            for (int i = 0; i < a.Count; i++)
            {
                issues.Add(a[i]);
            }
        }
        public void Remove(Issue a) => issues.Remove(a);
        public void RemoveAt(int index) => issues.RemoveAt(index);
        public int Count => issues.Count;

        public ObservableCollection<Issue> Issues { get => issues; set => issues = value; }

        public bool Contains(Issue a) => issues.Contains(a);
        public void Clear() => issues.Clear();

        public void Sort()
        {
            List<Issue> list = new List<Issue>(issues);
            list.Sort();
            issues.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                issues.Add(list[i]);
            }
        }
    }
}

