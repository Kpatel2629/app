﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace final_project
{
    class Empty : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var str = value as string;
            if ((str == "") || (value == null))
            {
                return new ValidationResult(false, "Fields are Empty");
            }
            else return ValidationResult.ValidResult;
        }
    }
}
