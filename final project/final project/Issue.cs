﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project
{
    public class Issue : IComparable
    {
        private string time;
        private string date;
        private Student student;
        private String bookStatus;

        public Issue() { }

        public Issue(string time, string dateTime, Student student, string status)
        {
            this.Time = time;
            this.Date = dateTime;
            this.Student = student;
            this.BookStatus = status;
        }

        public string Time { get => time; set => time = value; }
        public string Date { get => date; set => date = value; }
        public Student Student { get => student; set => student = value; }
        public string BookStatus { get => bookStatus; set => bookStatus = value; }

        public int CompareTo(object obj)
        {
            Issue a = (Issue)obj;
            return Date.CompareTo(a.Date);
        }
    }
}

