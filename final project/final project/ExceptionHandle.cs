﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace final_project
{
    class ExceptionHandle : Exception
    {

        public ExceptionHandle()
        {
        }

        public ExceptionHandle(string message) : base(message)
        {
        }

        public ExceptionHandle(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExceptionHandle(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

