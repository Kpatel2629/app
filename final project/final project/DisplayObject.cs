﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project
{
    public class DisplayObject
    {
        private string fullName;
        private string bookName;
        private string email;
        private string year;
        private string bookStatus;
        private AuthorName authorName;
        private BookCategory category;
        private DateTime issueDate;
        private string issueTime;
        public DisplayObject() { }
        
        public void Clear()
        {
            this.fullName = null;
            this.bookName = null;
            this.email = null;
            this.year = null; 
        }
        public string FullName { get => fullName; set => fullName = value; }
        public string BookName { get => bookName; set => bookName = value; }
        public string Email { get => email; set => email = value; }
        public string Year { get => year; set => year = value; }
        public string BookStatus { get => bookStatus; set => bookStatus = value; }
        public AuthorName AuthorName { get => authorName; set => authorName = value; }
        public BookCategory BookCategory { get => category; set => category = value; }
        public DateTime IssueDate { get => issueDate; set => issueDate = value; }
        public string IssueTime { get => issueTime; set => issueTime = value; }
    }
}
