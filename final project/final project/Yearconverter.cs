﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Globalization;

namespace final_project
{
    [ValueConversion(typeof(string), typeof(Brush))]
    public class Yearconverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return Brushes.Black;
            int i = int.Parse(value.ToString());
            if (i >= 2000)
            {
                return Brushes.Green;
            }
            else if (i < 2000)
            {
                return Brushes.Orange;
            }
            else
            {
                return Brushes.Black;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
