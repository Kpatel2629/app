﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project
{
    public interface IStudent
    {
        string FullName { get; set; }
        string BookName { get; set; }
        string Email { get; set; }
        int Year { get; set; }
        AuthorName AuthorName { get; set; }

        List<string> Category();
    }
}

