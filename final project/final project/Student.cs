﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace final_project
{
    [Serializable]
    public enum AuthorName
    {
        [XmlEnum("John Green")]
        JohnGreen,
        [XmlEnum("David Levithan")]
        DavidLevithan,
        [XmlEnum("R.M Drake")]
        RMDrake,
        [XmlEnum("Murakami")]
        Murakami,
        [XmlEnum("J.K Rowling")]
        JKRowling,
        [XmlEnum("Chetan Bhagat")]
        ChetanBhagat,
        [XmlEnum("Durjoy Dutta")]
        DurjoyDutta,
        [XmlEnum("Other")]
        Other,

    }
    
    [Serializable]
    public enum BookCategory
    {
        [XmlEnum("Novel")]
        Novel,
        [XmlEnum("Magazine")]
        Magazine,
        [XmlEnum("Article")]
        Article,
        [XmlEnum("Others")]
        Others,

    }
    public abstract class Student : IStudent
    {
        private string fullName;
        private string bookName;
        private string email;
        private int year;
        private AuthorName authorName;
        private BookCategory bookCategory;
        private string status;

        public string FullName { get => fullName; set => fullName = value; }
        public string BookName { get => bookName; set => bookName = value; }
        public string Email { get => email; set => email = value; }
        public int Year { get => year; set => year = value; }
        public AuthorName AuthorName { get => authorName; set => authorName = value; }
        public BookCategory BookCategory { get => bookCategory; set => bookCategory = value; }
        public string Status { get => status; set => status = value; }

        public abstract List<string> Category();
    }
}
