﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace final_project
{
    public partial class MainWindow : Window
    {
        private DisplayObject displayObject = new DisplayObject();
        private IssueList issueList = new IssueList();
        private DataHandling xml=null;
        private Queue<string> timeOptions = new Queue<string>();
        private Borrow borrow = new Borrow();
        private Purchase purchase = new Purchase();
        private DateTime filterDate = new DateTime();
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                xml = DataHandling.Instance();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException);
                MessageBox.Show(e.InnerException.ToString());
                Environment.Exit(0);
            }

            IssueList = xml.ReadIssue();
            if (issueList == null)
            {
                issueList = new IssueList();
            }
            timeOptions = xml.ReadTime();
            LoadDefaultValues();
            DataContext = this;
            cmbFilter.Items.Add("Borrow");
            cmbFilter.Items.Add("Purchase");
        }

        private void LoadDefaultValues()
        {

            cmbStatus.Items.Add("Borrow");
            cmbStatus.Items.Add("Purchase");
            displayObject.BookStatus = cmbStatus.Items.GetItemAt(0).ToString();


            cmbAuthorName.Items.Add(AuthorName.ChetanBhagat);
            cmbAuthorName.Items.Add(AuthorName.DavidLevithan);
            cmbAuthorName.Items.Add(AuthorName.DurjoyDutta);
            cmbAuthorName.Items.Add(AuthorName.JKRowling);
            cmbAuthorName.Items.Add(AuthorName.JohnGreen);
            cmbAuthorName.Items.Add(AuthorName.Murakami);
            cmbAuthorName.Items.Add(AuthorName.RMDrake);
            cmbAuthorName.Items.Add(AuthorName.Other);
            displayObject.AuthorName = AuthorName.JKRowling;


            cmbCategory.Items.Add(BookCategory.Novel);
            cmbCategory.Items.Add(BookCategory.Magazine);
            cmbCategory.Items.Add(BookCategory.Article);
            cmbCategory.Items.Add(BookCategory.Others);
            displayObject.BookCategory = BookCategory.Novel;


            displayObject.IssueDate = DateTime.Now;
        }

        public IssueList IssueList { get => issueList; set => issueList = value; }
        public DisplayObject DisplayObject { get => displayObject; set => displayObject = value; }
        public DateTime FilterDate { get => filterDate; set => filterDate = value; }
        public AuthorName AuthorName { get => AuthorName; set => AuthorName = value; }

        
        private void UpdateTimeOptions()
        {
            cmbIssueTime.Items.Clear();
            foreach (string time in timeOptions)
            {
                cmbIssueTime.Items.Add(time);
            }
            if (issueList != null)
            {
                for (int i = 0; i < issueList.Count; i++)
                {
                    if (issueList[i].Date == displayObject.IssueDate.ToString("MM/dd/yyyy"))
                    {
                        cmbIssueTime.Items.Remove(issueList[i].Time);
                    }
                }
            }
            cmbIssueTime.SelectedIndex = 0;
            displayObject.IssueTime = cmbIssueTime.SelectedItem.ToString();
        }
        int i;
        

        private bool ValidateData()
        {
            string s = displayObject.Year;
            bool isNum = int.TryParse(s, out i);
            if (txtFullName.Text == string.Empty)
            {
                txtFullName.BorderBrush = Brushes.Red;
                MessageBox.Show("Please enter valid name.");
                return false;
            }
            else if(txtBookName.Text == string.Empty)
            {
                txtBookName.BorderBrush = Brushes.Red;
                MessageBox.Show("Please enter valid book name.");
                return false;
            }
            else if (txtEmail.Text == string.Empty)
            {
                txtEmail.BorderBrush = Brushes.Red;
                MessageBox.Show("Please enter valid email.");
                return false;
            }
            else if (displayObject.Year == string.Empty || !isNum || i>2018)
            {
                txtPublishYear.BorderBrush = Brushes.Red;
                MessageBox.Show("Please enter valid numbers.");
                return false;
            }
            else if (cmbAuthorName.SelectedIndex==-1)
            {
                brdrAuthorName.BorderBrush = Brushes.Red;
                MessageBox.Show("Please select author.");
                return false;
            }
            else if (cmbCategory.SelectedIndex == -1)
            {
                brdrCategory.BorderBrush = Brushes.Red;
                MessageBox.Show("Please select category.");
                return false;
            }
            else if (cmbIssueTime.SelectedIndex == -1)
            {
                brdrIssueTime.BorderBrush = Brushes.Red;
                MessageBox.Show("Please select Issue Time.");
                return false;
            }
            else if (cmbStatus.SelectedIndex == -1)
            {
                brdrStatus.BorderBrush = Brushes.Red;
                MessageBox.Show("Please select status.");
                return false;
            }
            else
            {
                return true;
            }
            
        }
        private void ResetData()
        {
            displayObject.Clear();
            txtFullName.Text = displayObject.FullName;
            txtBookName.Text = displayObject.BookName;
            txtEmail.Text = displayObject.Email;
            txtPublishYear.Text = displayObject.Year;
            cmbStatus.SelectedIndex = 0;
            cmbAuthorName.SelectedIndex = 0;
            cmbCategory.SelectedIndex = 0;
            dtpkrIssueDate.Text = displayObject.IssueDate.ToString("dd/MM/yyyy");
            UpdateTimeOptions();
        }

        private void SaveAppointment()
        {
            Issue a = new Issue
            {
                Date = displayObject.IssueDate.ToString("dd/MM/yyyy"),
                Time = displayObject.IssueTime
            };

            switch (displayObject.BookStatus)
            {
                case "Borrow":
                    a.Student = new Borrow();
                    break;
                case "Purchase":
                    a.Student = new Purchase();
                    break;
            }
            a.Student.FullName = displayObject.FullName;
            a.Student.BookName = displayObject.BookName;
            a.Student.Email = displayObject.Email;
            a.Student.Year = int.Parse(displayObject.Year);
            a.Student.AuthorName = displayObject.AuthorName;
            a.Student.Status = displayObject.BookStatus;
            a.Student.BookCategory = displayObject.BookCategory;
            issueList.Add(a);
            xml.WriteAppointments(issueList);
        }
        private void BtnIssue_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                SaveAppointment();
                ResetData();
                InitializeComponent();
            }
            else return;
        }

        private void BtnReturn_Click(object sender, RoutedEventArgs e)
        {
            
            if (dataGrid.SelectedIndex >= 0)
            {
                issueList.RemoveAt(dataGrid.SelectedIndex);
                xml.WriteAppointments(issueList);
            }
            
        }

        
        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {

            dataGrid.ItemsSource = issueList.Issues;
        }

        private void DateChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateTimeOptions();
        }
        
        private void TxtFullName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        

        private void BtnFilter_Click(object sender, RoutedEventArgs e)
        {
            var filterValue = cmbFilter.Text;
            var query = from a in issueList.Issues
                        where a.Student.Status == filterValue
                        select a;
            dataGrid.ItemsSource = query.ToList();
        }

        private void DtpkrIssueDate_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void CmbFilter_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void TxtFullName_GotFocus(object sender, RoutedEventArgs e)
        {
            txtFullName.BorderBrush = Brushes.Black;
        }

        private void TxtBookName_GotFocus(object sender, RoutedEventArgs e)
        {
            txtBookName.BorderBrush = Brushes.Black;
        }

        private void TxtEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            txtEmail.BorderBrush = Brushes.Black;
        }

        private void TxtPublishYear_GotFocus(object sender, RoutedEventArgs e)
        {
            txtPublishYear.BorderBrush = Brushes.Red;
        }
    }
    
}
