﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Globalization;

namespace final_project
{
    class Yearrule : ValidationRule
    {
        private int min;
        public int Min { get => min; set => min = value; }
        

        int year = 0;
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            
            if (!int.TryParse((string)value, out year))
            {
                return new ValidationResult(false, "Invalid data type");
            }
            if (year < min)
            {
                return new ValidationResult(false, string.Format("Year is out of range {0}", min));
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }
}

