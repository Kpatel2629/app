﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace final_project
{
    public class DataHandling
    {
        private const string issueList = "issue.xml";
        private const string issueTime = "time.xml";
        private XmlReaderSettings readerSettings = null;

        private static readonly DataHandling instance = new DataHandling();
        private List<Type> statusTypes;

        private DataHandling()
        {
            statusTypes = new List<Type>
            {
                typeof(Borrow),
                typeof(Purchase),
                typeof(AuthorName),
                typeof(BookCategory)
            };
            ManageData();
        }
        public static DataHandling Instance()
        {
            return instance;
        }

        public IssueList ReadIssue()
        {
            IssueList appointmentList = null;
            XmlSerializer serializer = new XmlSerializer(typeof(IssueList), statusTypes.ToArray());
            StreamReader reader = new StreamReader(issueList);

            try
            {
                appointmentList = (IssueList)serializer.Deserialize(reader);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                reader.Close();
            }
            return appointmentList;
        }

        public void WriteAppointments(IssueList issueLists)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(IssueList), statusTypes.ToArray());
            TextWriter writer = new StreamWriter(issueList);
            serializer.Serialize(writer, issueLists);
            writer.Close();
        }

        public Queue<string> ReadTime()
        {
            Queue<string> timeOptions = new Queue<string>();
            XmlReader reader = XmlReader.Create(issueTime, readerSettings);
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    timeOptions.Enqueue(reader.Value);
                }
            }
            reader.Close();
            return timeOptions;
        }

        public Dictionary<string, List<string>> ReadCategory()
        {
            Dictionary<string, List<string>> categories = new Dictionary<string, List<string>>();
            List<string> purchasestatus = new List<string>();
            List<string> borrowstatus = new List<string>();

            XmlDocument xmlDocument = new XmlDocument();

            XmlNodeList borrowNodeList = xmlDocument.GetElementsByTagName("Borrow")[0].ChildNodes;
            XmlNodeList purchaseNodeList = xmlDocument.GetElementsByTagName("Purchase")[0].ChildNodes;

            for (int i = 0; i < borrowNodeList.Count; i++)
            {
                borrowstatus.Add(borrowNodeList[i].InnerXml);
            }
            for (int i = 0; i < purchaseNodeList.Count; i++)
            {
                purchasestatus.Add(purchaseNodeList[i].InnerXml);
            }
            categories.Add("Borrow", borrowstatus);
            categories.Add("Purchase", purchasestatus);
            return categories;
        }

        private void ManageData()
        {
            if (!File.Exists(issueList))
            {
                //Free resources
                using (File.Create(issueList)) { }
            }
            if (!File.Exists(issueTime)) throw new ExceptionHandle();
            readerSettings = new XmlReaderSettings
            {
                IgnoreWhitespace = true,
                IgnoreComments = true
            };

        }
    }
}
